import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  group('assessment app', () {
    final onboardingButton = find.byValueKey('joinButton');
    final signInButton = find.byValueKey('signInButton');

    late FlutterDriver driver;

    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    tearDownAll(() async {
      await driver.close();
    });

    test('signIn', () async{
      await driver.tap(onboardingButton);
      await driver.tap(signInButton);
      await driver.waitFor(find.text('Home'));

    });
  });
}
