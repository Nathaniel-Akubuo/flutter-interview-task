import 'package:flutter/material.dart';
import 'package:flutter_interview_task/constants/colors.dart';

class RoundedButton extends StatelessWidget {
  final Widget? child;
  final VoidCallback? onPressed;
  final width;
  final height;
  final color;

  RoundedButton(
      {this.onPressed,
      this.child,
      this.width,
      this.height,
      this.color,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: DecoratedBox(
        decoration: BoxDecoration(
            color: color,
            gradient: color == null ? kLinearGradient : null,
            borderRadius: BorderRadius.all(Radius.circular(14))),
        child: MaterialButton(
          onPressed: onPressed,
          minWidth: width ?? double.infinity,
          height: height ?? 58,
          child: child,
        ),
      ),
    );
  }
}
