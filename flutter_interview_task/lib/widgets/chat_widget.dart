import 'package:flutter/material.dart';
import 'package:flutter_interview_task/constants/colors.dart';
import 'package:flutter_interview_task/constants/ui_helpers.dart';
import 'package:flutter_interview_task/models/chat_model.dart';

class ChatWidget extends StatelessWidget {
  final ChatModel? chatModel;

  ChatWidget({this.chatModel});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: kPadding,
      child: Row(
        children: [
          CircleAvatar(
            radius: 29,
            backgroundImage: chatModel!.image,
          ),
          horizontalSpaceSmall,
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(
                    chatModel!.name,
                    style: kTitleTextStyle.copyWith(fontSize: 20),
                  ),
                  horizontalSpaceLarge,
                  Visibility(
                      visible: chatModel!.unread,
                      child: Container(
                        height: 10,
                        width: 10,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: kOrange),
                      ))
                ],
              ),
              Text(
                '${chatModel!.message} . ${chatModel!.time}',
                style: kSubtitleTextStyle.copyWith(color: kBlack),
              )
            ],
          )
        ],
      ),
    );
  }
}
