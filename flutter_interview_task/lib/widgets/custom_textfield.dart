import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:flutter_interview_task/constants/colors.dart';

// ignore: must_be_immutable
class CustomTextField extends StatefulWidget {
  final String hintText;
  final String labelText;
  final int maxLines;
  final bool isPasswordTextField;
  final TextInputAction? textInputAction;
  final TextInputType? textInputType;
  final TextEditingController controller;
  final validator;
  bool obscureText;
  final prefixIcon;
  final hintStyle;
  final isSearch;
  final suffixIcon;
  final height;
  final color;

  CustomTextField.password(
      {this.hintText = '',
      required this.labelText,
      this.maxLines = 1,
      this.isPasswordTextField = true,
      this.textInputAction,
      this.textInputType,
      required this.controller,
      this.obscureText = true,
      this.prefixIcon,
      this.hintStyle,
      this.isSearch = false,
      this.suffixIcon,
      this.height,
      this.color,
      this.validator});

  CustomTextField.regular(
      {this.hintText = '',
      required this.labelText,
      this.maxLines = 1,
      this.isPasswordTextField = false,
      this.textInputAction,
      this.textInputType,
      required this.controller,
      this.obscureText = false,
      this.prefixIcon,
      this.hintStyle,
      this.isSearch = false,
      this.suffixIcon,
      this.height,
      this.color,
      this.validator});

  CustomTextField.search(
      {this.hintText = '',
      required this.labelText,
      this.maxLines = 1,
      this.prefixIcon,
      this.isPasswordTextField = false,
      this.textInputAction,
      this.textInputType,
      required this.controller,
      this.obscureText = false,
      this.hintStyle,
      this.isSearch = true,
      this.suffixIcon,
      this.height,
      this.color,
      this.validator});

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 10, bottom: 3),
      height: widget.height ?? 58,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(14)),
          color: widget.color ?? kLightGrey),
      child: TextFormField(
        style: kSubtitleTextStyle.copyWith(color: kBlack),
        controller: widget.controller,
        validator: widget.validator,
        obscureText: widget.obscureText,
        textInputAction: widget.textInputAction,
        keyboardType: widget.textInputType,
        decoration: InputDecoration(
          floatingLabelBehavior: FloatingLabelBehavior.never,
          prefixIcon: widget.prefixIcon,
          suffixIcon: widget.isSearch
              ? widget.suffixIcon ??
                  Icon(IconlyLight.filter, color: kMediumGrey)
              : widget.isPasswordTextField
                  ? IconButton(
                      icon: widget.obscureText
                          ? Icon(IconlyLight.hide, color: kMediumGrey)
                          : Icon(IconlyLight.show, color: kMediumGrey),
                      onPressed: () {
                        setState(() {
                          widget.obscureText = !widget.obscureText;
                        });
                      },
                    )
                  : null,
          border: InputBorder.none,
          labelText: widget.labelText,
          labelStyle:
              kSubtitleTextStyle.copyWith(fontSize: 17, color: kMediumGrey),
          hintText: widget.hintText,
          hintStyle: widget.hintStyle ??
              kSubtitleTextStyle.copyWith(fontSize: 17, color: kMediumGrey),
        ),
      ),
    );
  }
}
