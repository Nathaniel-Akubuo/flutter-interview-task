import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:flutter_interview_task/constants/colors.dart';
import 'package:flutter_interview_task/constants/ui_helpers.dart';
import 'package:flutter_interview_task/models/listing_model.dart';

class ListingContainer extends StatelessWidget {
  final ListingModel listingModel;

  ListingContainer(this.listingModel);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Stack(
          alignment: Alignment.topRight,
          children: [
            Container(
              height: 125,
              width: 180,
              child: ClipRRect(
                borderRadius: kBorderRadius,
                child: FittedBox(fit: BoxFit.fill, child: listingModel.image),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ClipRRect(
                borderRadius: k7pxBorderRadius,
                child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                  child: Container(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    height: 25,
                    width: 50,
                    decoration: BoxDecoration(
                      borderRadius: k7pxBorderRadius,
                      color: kLightGrey.withOpacity(0.2),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Icon(Icons.star, color: kYellow, size: 12),
                        Text(
                          listingModel.rating,
                          style: kSubtitleTextStyle.copyWith(
                              color: kYellow, fontSize: 10),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        verticalSpaceSmall,
        Text(
          listingModel.name,
          style: kTitleTextStyle.copyWith(fontSize: 17),
        ),
        Container(
          width: 180,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                child: Row(
                  children: [
                    Icon(
                      IconlyLight.location,
                      color: kMediumGrey,
                      size: 18,
                    ),
                    Text('${listingModel.location} km from you',
                        style: kSubtitleTextStyle.copyWith(fontSize: 12)),
                  ],
                ),
              ),
              Container(
                width: 48,
                alignment: Alignment.center,
                height: 25,
                child: Text(
                  r'$'+'${listingModel.rate}/h',
                  style: kSubtitleTextStyle.copyWith(
                      color: Colors.white, fontSize: 10),
                ),
                decoration: BoxDecoration(
                    borderRadius: k7pxBorderRadius, color: Color(0xff2b2b2b)),
              )
            ],
          ),
        )
      ],
    );
  }
}
