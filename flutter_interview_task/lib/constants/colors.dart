import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const kLinearGradient = LinearGradient(colors: [
  Color(0xffFB724C),
  Color(0xffFE8F4B),
]);

const kBlack = Color(0xff161616);
const kLightGrey = Color(0xffF0F0F0);
const kMediumGrey = Color(0xffAEAEB2);
const kDarkGrey = Color(0xffB0B0B0);
const kYellow = Color(0xffFFCB55);
const kOrange = Color(0xffFB724C);
const kGreen = Color(0xff5AD439);
const kTitleTextStyle = TextStyle(
    fontFamily: 'Poppins',
    fontWeight: FontWeight.bold,
    fontSize: 34,
    color: Colors.black);
const kSubtitleTextStyle =
    TextStyle(fontFamily: 'Poppins', fontSize: 17, color: kMediumGrey);

const kPadding = EdgeInsets.symmetric(horizontal: 15, vertical: 10);
const kBorderRadius = BorderRadius.all(Radius.circular(14));
const k7pxBorderRadius = BorderRadius.all(Radius.circular(7));
