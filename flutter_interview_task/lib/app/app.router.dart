// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// StackedRouterGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import '../models/chat_model.dart';
import '../models/listing_model.dart';
import '../ui/bottom_nav_bar_view/bottom_nav_bar_view.dart';
import '../ui/chat/chats_view.dart';
import '../ui/chat_details_view/chat_details_view.dart';
import '../ui/details/profile_details_view.dart';
import '../ui/home/home_view.dart';
import '../ui/moments/moments_view.dart';
import '../ui/onboarding/onboarding_view.dart';
import '../ui/profile/profile_view.dart';
import '../ui/sign-up/sign_up_view.dart';

class Routes {
  static const String onBoardingView = '/on-boarding-view';
  static const String signUpView = '/sign-up-view';
  static const String homeView = '/home-view';
  static const String chatsView = '/chats-view';
  static const String momentsView = '/moments-view';
  static const String profileView = '/profile-view';
  static const String profileDetailsView = '/profile-details-view';
  static const String bottomNavBarView = '/bottom-nav-bar-view';
  static const String chatDetailsView = '/chat-details-view';
  static const all = <String>{
    onBoardingView,
    signUpView,
    homeView,
    chatsView,
    momentsView,
    profileView,
    profileDetailsView,
    bottomNavBarView,
    chatDetailsView,
  };
}

class StackedRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.onBoardingView, page: OnBoardingView),
    RouteDef(Routes.signUpView, page: SignUpView),
    RouteDef(Routes.homeView, page: HomeView),
    RouteDef(Routes.chatsView, page: ChatsView),
    RouteDef(Routes.momentsView, page: MomentsView),
    RouteDef(Routes.profileView, page: ProfileView),
    RouteDef(Routes.profileDetailsView, page: ProfileDetailsView),
    RouteDef(Routes.bottomNavBarView, page: BottomNavBarView),
    RouteDef(Routes.chatDetailsView, page: ChatDetailsView),
  ];
  @override
  Map<Type, StackedRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, StackedRouteFactory>{
    OnBoardingView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => OnBoardingView(),
        settings: data,
      );
    },
    SignUpView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => SignUpView(),
        settings: data,
      );
    },
    HomeView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => HomeView(),
        settings: data,
      );
    },
    ChatsView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => ChatsView(),
        settings: data,
      );
    },
    MomentsView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => MomentsView(),
        settings: data,
      );
    },
    ProfileView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => ProfileView(),
        settings: data,
      );
    },
    ProfileDetailsView: (data) {
      var args = data.getArgs<ProfileDetailsViewArguments>(nullOk: false);
      return MaterialPageRoute<dynamic>(
        builder: (context) => ProfileDetailsView(args.listingModel),
        settings: data,
      );
    },
    BottomNavBarView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => BottomNavBarView(),
        settings: data,
      );
    },
    ChatDetailsView: (data) {
      var args = data.getArgs<ChatDetailsViewArguments>(nullOk: false);
      return MaterialPageRoute<dynamic>(
        builder: (context) => ChatDetailsView(args.chatModel),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Arguments holder classes
/// *************************************************************************

/// ProfileDetailsView arguments holder class
class ProfileDetailsViewArguments {
  final ListingModel listingModel;
  ProfileDetailsViewArguments({required this.listingModel});
}

/// ChatDetailsView arguments holder class
class ChatDetailsViewArguments {
  final ChatModel chatModel;
  ChatDetailsViewArguments({required this.chatModel});
}
