import 'package:flutter_interview_task/ui/bottom_nav_bar_view/bottom_nav_bar_view.dart';
import 'package:flutter_interview_task/ui/chat/chats_view.dart';
import 'package:flutter_interview_task/ui/chat_details_view/chat_details_view.dart';
import 'package:flutter_interview_task/ui/details/profile_details_view.dart';
import 'package:flutter_interview_task/ui/home/home_view.dart';
import 'package:flutter_interview_task/ui/moments/moments_view.dart';
import 'package:flutter_interview_task/ui/onboarding/onboarding_view.dart';
import 'package:flutter_interview_task/ui/profile/profile_view.dart';
import 'package:flutter_interview_task/ui/sign-up/sign_up_view.dart';
import 'package:stacked/stacked_annotations.dart';
import 'package:stacked_services/stacked_services.dart';

@StackedApp(
  routes: [
    MaterialRoute(page: OnBoardingView),
    MaterialRoute(page: SignUpView),
    MaterialRoute(page: HomeView),
    MaterialRoute(page: ChatsView),
    MaterialRoute(page: MomentsView),
    MaterialRoute(page: ProfileView),
    MaterialRoute(page: ProfileDetailsView),
    MaterialRoute(page: BottomNavBarView),
    MaterialRoute(page: ChatDetailsView),
  ],
  dependencies: [
    LazySingleton(classType: NavigationService),
  ],
)
class AppSetup {}
