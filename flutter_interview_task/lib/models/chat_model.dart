class ChatModel {
  final image;
  final name;
  final time;
  bool unread;
  final message;

  ChatModel(
      {this.image, this.name, this.time, this.unread = false, this.message});
}
