import 'package:flutter/material.dart';

class ListingModel {
  final location;
  final Image? image;
  final name;
  final rate;
  final rating;

  ListingModel({this.location, this.image, this.name, this.rate, this.rating});
}
