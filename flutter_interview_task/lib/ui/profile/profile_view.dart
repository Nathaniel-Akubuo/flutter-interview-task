import 'package:flutter/material.dart';
import 'package:flutter_interview_task/ui/profile/profile_view_model.dart';
import 'package:stacked/stacked.dart';

class ProfileView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ProfileViewModel>.reactive(
        builder: (context, model, child) => Scaffold(),
        viewModelBuilder: () => ProfileViewModel());
  }
}
