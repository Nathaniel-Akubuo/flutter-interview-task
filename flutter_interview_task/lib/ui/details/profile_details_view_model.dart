import 'package:flutter_interview_task/app/app.locator.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class ProfileDetailsViewModel extends BaseViewModel {
  final _navigationService = locator<NavigationService>();
  bool expanded = false;

  void back() => _navigationService.back();

  void toggleExpanded() {
    expanded = !expanded;
    notifyListeners();
  }
}
