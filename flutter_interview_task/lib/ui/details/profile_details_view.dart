import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:flutter_interview_task/constants/colors.dart';
import 'package:flutter_interview_task/constants/ui_helpers.dart';
import 'package:flutter_interview_task/models/listing_model.dart';
import 'package:flutter_interview_task/ui/details/profile_details_view_model.dart';
import 'package:flutter_interview_task/widgets/rounded_button.dart';
import 'package:stacked/stacked.dart';

class ProfileDetailsView extends StatelessWidget {
  final ListingModel listingModel;

  ProfileDetailsView(this.listingModel);

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    return ViewModelBuilder<ProfileDetailsViewModel>.reactive(
        builder: (context, model, child) => SafeArea(
              child: Scaffold(
                body: Stack(
                  children: [
                    Positioned(
                      top: 0,
                      child: Container(
                        height: mediaQuery.height * 0.4,
                        width: mediaQuery.width,
                        child: FittedBox(
                          fit: BoxFit.fill,
                          child: listingModel.image,
                        ),
                      ),
                    ),
                    Positioned(
                      top: 15,
                      child: Container(
                        width: mediaQuery.width,
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                              onTap: model.back,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(100),
                                child: BackdropFilter(
                                  filter:
                                      ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                                  child: Container(
                                    height: 44,
                                    width: 44,
                                    child:
                                        Icon(Icons.close, color: Colors.white),
                                    color: kLightGrey.withOpacity(0.2),
                                  ),
                                ),
                              ),
                            ),
                            ClipRRect(
                              borderRadius: BorderRadius.circular(20),
                              child: BackdropFilter(
                                filter:
                                    ImageFilter.blur(sigmaX: 20, sigmaY: 20),
                                child: Container(
                                  padding: const EdgeInsets.all(12),
                                  height: 44,
                                  width: 104,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Verified',
                                        style: kTitleTextStyle.copyWith(
                                            color: Colors.white, fontSize: 13),
                                      ),
                                      Icon(IconlyBold.tickSquare,
                                          color: Colors.white, size: 20)
                                    ],
                                  ),
                                  color: kLightGrey.withOpacity(0.2),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      top: mediaQuery.height * 0.35,
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: const BorderRadius.vertical(
                                top: Radius.circular(24))),
                        child: SingleChildScrollView(
                          physics: BouncingScrollPhysics(),
                          padding: kPadding,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Center(
                                child: Text(listingModel.name,
                                    style:
                                        kTitleTextStyle.copyWith(fontSize: 28)),
                              ),
                              verticalSpaceSmall,
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    height: 15,
                                    child: Row(
                                      children: [
                                        Text(
                                          '${listingModel.rate}\$',
                                          style: kTitleTextStyle.copyWith(
                                              fontSize: 13),
                                        ),
                                        Text(
                                          '/hr',
                                          style: kSubtitleTextStyle.copyWith(
                                              fontSize: 13),
                                        ),
                                        horizontalSpaceTiny,
                                        VerticalDivider(color: kBlack),
                                        horizontalSpaceTiny,
                                        Text(
                                          listingModel.location,
                                          style: kTitleTextStyle.copyWith(
                                              fontSize: 13),
                                        ),
                                        Text(
                                          ' km',
                                          style: kSubtitleTextStyle.copyWith(
                                              fontSize: 13),
                                        ),
                                        horizontalSpaceTiny,
                                        VerticalDivider(color: kBlack),
                                        horizontalSpaceTiny,
                                        Text(
                                          listingModel.rating,
                                          style: kTitleTextStyle.copyWith(
                                              fontSize: 13),
                                        ),
                                        Icon(Icons.star,
                                            color: kMediumGrey, size: 18),
                                        horizontalSpaceTiny,
                                        VerticalDivider(color: kBlack),
                                        horizontalSpaceTiny,
                                        Text(
                                          '450',
                                          style: kTitleTextStyle.copyWith(
                                              fontSize: 13),
                                        ),
                                        Text(
                                          ' walks',
                                          style: kSubtitleTextStyle.copyWith(
                                              fontSize: 13),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              verticalSpaceMedium,
                              Divider(color: kMediumGrey),
                              verticalSpaceRegular,
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  RoundedButton(
                                    width: 99.0,
                                    height: 41.0,
                                    color: kBlack,
                                    child: Text(
                                      'About',
                                      style: kSubtitleTextStyle.copyWith(
                                          fontSize: 13, color: Colors.white),
                                    ),
                                  ),
                                  RoundedButton(
                                    color: kLightGrey,
                                    width: 99.0,
                                    height: 41.0,
                                    child: Text(
                                      'Location',
                                      style: kSubtitleTextStyle.copyWith(
                                          color: kDarkGrey),
                                    ),
                                  ),
                                  RoundedButton(
                                    color: kLightGrey,
                                    width: 99.0,
                                    height: 41.0,
                                    child: Text(
                                      'Reviews',
                                      style: kSubtitleTextStyle.copyWith(
                                          color: kDarkGrey),
                                    ),
                                  ),
                                ],
                              ),
                              verticalSpaceSmall,
                              verticalSpaceSmall,
                              Row(
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text('Age',
                                          style: kSubtitleTextStyle.copyWith(
                                              color: kDarkGrey)),
                                      Text('30 years',
                                          style: kTitleTextStyle.copyWith(
                                              color: kBlack, fontSize: 17)),
                                    ],
                                  ),
                                  horizontalSpaceLarge,
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text('Experience',
                                          style: kSubtitleTextStyle.copyWith(
                                              color: kDarkGrey)),
                                      Text('11 months',
                                          style: kTitleTextStyle.copyWith(
                                              color: kBlack, fontSize: 17)),
                                    ],
                                  ),
                                ],
                              ),
                              verticalSpaceRegular,
                              Container(
                                child: Text(
                                  'Alex has loved dogs since childhood. He is '
                                  'currently a veterinary student. Visits '
                                  'the dog shelter weekly to help out in any way he can',
                                  maxLines: model.expanded ? 100 : 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: kSubtitleTextStyle.copyWith(
                                      color: kMediumGrey, fontSize: 15),
                                ),
                              ),
                              verticalSpaceTiny,
                              GestureDetector(
                                onTap: model.toggleExpanded,
                                child: Text(
                                  'Read more',
                                  style: kSubtitleTextStyle.copyWith(
                                      color: kOrange, fontSize: 13),
                                ),
                              ),
                              RoundedButton(
                                child: Text(
                                  'Check schedule',
                                  style: kTitleTextStyle.copyWith(
                                      color: Colors.white, fontSize: 17),
                                ),
                              ),
                            ],
                          ),
                        ),
                        height: mediaQuery.height * 0.6,
                        width: mediaQuery.width,
                      ),
                    ),
                  ],
                ),
              ),
            ),
        viewModelBuilder: () => ProfileDetailsViewModel());
  }
}
