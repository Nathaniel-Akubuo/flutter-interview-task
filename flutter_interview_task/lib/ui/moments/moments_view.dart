import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import 'moments_view_model.dart';

class MomentsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<MomentsViewModel>.reactive(
        builder: (context, model, child) => Scaffold(),
        viewModelBuilder: () => MomentsViewModel());
  }
}
