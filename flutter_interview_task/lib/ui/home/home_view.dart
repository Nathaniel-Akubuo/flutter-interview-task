import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:flutter_interview_task/constants/colors.dart';
import 'package:flutter_interview_task/constants/ui_helpers.dart';
import 'package:flutter_interview_task/ui/home/home_view_model.dart';
import 'package:flutter_interview_task/widgets/custom_textfield.dart';
import 'package:flutter_interview_task/widgets/listing_container.dart';
import 'package:flutter_interview_task/widgets/rounded_button.dart';
import 'package:stacked/stacked.dart';

class HomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeViewModel>.reactive(
        builder: (context, model, child) => Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                leadingWidth: double.infinity,
                leading: Padding(
                  padding: EdgeInsets.only(left: 15),
                  child: Text('Home',
                      style: kTitleTextStyle.copyWith(fontSize: 30)),
                ),
                backgroundColor: Colors.white,
                elevation: 0,
                centerTitle: false,
                bottom: PreferredSize(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Explore dog walkers', style: kSubtitleTextStyle),
                        RoundedButton(
                          key: ValueKey('homeButton'),
                          height: 41.0,
                          width: 100.0,
                          child: Row(
                            children: [
                              Icon(Icons.add, color: Colors.white),
                              Text(
                                'Book a walk',
                                style: kSubtitleTextStyle.copyWith(
                                    fontSize: 10, color: Colors.white),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  preferredSize: Size.fromHeight(65),
                ),
              ),
              body: SingleChildScrollView(
                padding: kPadding,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomTextField.search(
                      hintStyle: kSubtitleTextStyle.copyWith(fontSize: 17),
                      controller: model.controller,
                      prefixIcon: Icon(
                        IconlyLight.location,
                        color: kMediumGrey,
                      ),
                      labelText: 'Kiyv, Ukraine',
                    ),
                    verticalSpaceMedium,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Near you',
                            style: kTitleTextStyle.copyWith(fontSize: 30)),
                        Text(
                          'View more',
                          style: kSubtitleTextStyle.copyWith(
                              fontSize: 15,
                              color: Colors.black,
                              decoration: TextDecoration.underline),
                        )
                      ],
                    ),
                    verticalSpaceSmall,
                    Container(
                        height: 200,
                        child: ListView.separated(
                            physics: BouncingScrollPhysics(),
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, index) => GestureDetector(
                                onTap: () => model.navigateToProfileDetails(
                                    model.list[index]),
                                child: ListingContainer(model.list[index])),
                            separatorBuilder: (context, i) => SizedBox(
                                  width: 22,
                                ),
                            itemCount: model.list.length)),
                    Divider(
                      color: kDarkGrey,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Suggested',
                            style: kTitleTextStyle.copyWith(fontSize: 30)),
                        Text(
                          'View more',
                          style: kSubtitleTextStyle.copyWith(
                              fontSize: 15,
                              color: Colors.black,
                              decoration: TextDecoration.underline),
                        )
                      ],
                    ),
                    verticalSpaceSmall,
                    Container(
                        height: 200,
                        child: ListView.separated(
                            physics: BouncingScrollPhysics(),
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, index) => GestureDetector(
                                onTap: () => model.navigateToProfileDetails(
                                    model.list2[index]),
                                child: ListingContainer(model.list2[index])),
                            separatorBuilder: (context, i) => SizedBox(
                                  width: 22,
                                ),
                            itemCount: model.list.length)),
                  ],
                ),
              ),
            ),
        viewModelBuilder: () => HomeViewModel());
  }
}
