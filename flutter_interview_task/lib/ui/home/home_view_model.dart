import 'package:flutter/cupertino.dart';
import 'package:flutter_interview_task/app/app.locator.dart';
import 'package:flutter_interview_task/app/app.router.dart';
import 'package:flutter_interview_task/models/listing_model.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class HomeViewModel extends BaseViewModel {
  var controller = TextEditingController();
  final _navigationService = locator<NavigationService>();

  List<ListingModel> list = [
    ListingModel(
        location: '7',
        image: Image.asset('assets/IMAGE (1).png'),
        name: 'Alex Murray',
        rate: '1',
        rating: '4.2'),
    ListingModel(
        location: '14',
        image: Image.asset('assets/dog1.jpg'),
        name: 'Mark Greene',
        rate: '2',
        rating: '5.0'),
    ListingModel(
        location: '4',
        image: Image.asset('assets/dog2.jpg'),
        name: 'Trina Kain',
        rate: '3',
        rating: '4.5')
  ];
  List<ListingModel> list2 = [
    ListingModel(
        location: '14',
        image: Image.asset('assets/dog3.jpg'),
        name: 'Mason York',
        rate: '4',
        rating: '4.6'),
    ListingModel(
        location: '14',
        image: Image.asset('assets/dog4.jpg'),
        name: 'John Doe',
        rate: '5',
        rating: '4.7'),
    ListingModel(
        location: '14',
        image: Image.asset('assets/dog5.jpg'),
        name: 'John Doe',
        rate: '6',
        rating: '4.0')
  ];

  void navigateToProfileDetails(listingModel) =>
      _navigationService.navigateTo(Routes.profileDetailsView, arguments: ProfileDetailsViewArguments(listingModel: listingModel));
}
