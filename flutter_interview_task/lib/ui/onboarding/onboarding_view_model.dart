import 'package:flutter_interview_task/app/app.locator.dart';
import 'package:flutter_interview_task/app/app.router.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class OnBoardingViewModel extends BaseViewModel {
  final _navigationService = locator<NavigationService>();

  void navigateToSignUp() => _navigationService.navigateTo(Routes.signUpView);
}
