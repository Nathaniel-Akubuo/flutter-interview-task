import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_interview_task/constants/colors.dart';
import 'package:flutter_interview_task/constants/ui_helpers.dart';
import 'package:flutter_interview_task/ui/onboarding/onboarding_view_model.dart';
import 'package:flutter_interview_task/widgets/rounded_button.dart';
import 'package:stacked/stacked.dart';

class OnBoardingView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    return ViewModelBuilder<OnBoardingViewModel>.reactive(
        builder: (context, model, child) => Scaffold(
              backgroundColor: Colors.transparent,
              body: Stack(
                children: [
                  Container(
                    height: mediaQuery.height,
                    width: mediaQuery.width,
                    child: FittedBox(
                      fit: BoxFit.cover,
                      child: Image.asset(
                        'assets/ONBOARDING PICTURE.png',
                      ),
                    ),
                  ),
                  Container(
                    height: mediaQuery.height,
                    width: mediaQuery.width,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            Colors.transparent,
                            Colors.black,
                          ]),
                    ),
                  ),
                  Positioned(
                    top: 45,
                    left: 16,
                    child: Row(
                      children: [
                        Image.asset('assets/dog-paw.png'),
                        Image.asset('assets/woo dog.png'),
                      ],
                    ),
                  ),
                  Positioned(
                    top: mediaQuery.height * 0.6,
                    child: Container(
                      width: mediaQuery.width,
                      padding: kPadding,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                height: 30,
                                width: 30,
                                child: Center(
                                  child: Text(
                                    '1',
                                    style: kSubtitleTextStyle.copyWith(
                                        color: kBlack, fontSize: 17),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle),
                              ),
                              horizontalSpaceTiny,
                              Container(
                                  height: 2, width: 20, color: Colors.white),
                              horizontalSpaceTiny,
                              Container(
                                height: 30,
                                width: 30,
                                child: Center(
                                  child: Text(
                                    '2',
                                    style: kSubtitleTextStyle.copyWith(
                                        color: Colors.white, fontSize: 17),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                    color: kDarkGrey, shape: BoxShape.circle),
                              ),
                              horizontalSpaceTiny,
                              Container(
                                height: 2,
                                width: 20,
                                color: Colors.white,
                              ),
                              horizontalSpaceTiny,
                              Container(
                                height: 30,
                                width: 30,
                                child: Center(
                                  child: Text(
                                    '3',
                                    style: kSubtitleTextStyle.copyWith(
                                        color: Colors.white, fontSize: 17),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                    color: kDarkGrey, shape: BoxShape.circle),
                              ),
                            ],
                          ),
                          verticalSpaceRegular,
                          Text(
                            'Too tired to walk your dog? Let’s help you!',
                            textAlign: TextAlign.center,
                            style: kTitleTextStyle.copyWith(
                                fontSize: 22, color: Colors.white),
                          ),
                          verticalSpaceRegular,
                          RoundedButton(
                            onPressed: model.navigateToSignUp,
                            key: ValueKey('joinButton'),
                            child: Text(
                              'Join our community',
                              style: kTitleTextStyle.copyWith(
                                  color: Colors.white, fontSize: 17),
                            ),
                          ),
                          verticalSpaceRegular,
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Center(
                              child: RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                  text: 'Already a user? ',
                                  style: kSubtitleTextStyle.copyWith(
                                      fontSize: 15, color: Colors.white),
                                  children: [
                                    TextSpan(
                                      text: 'Sign In',
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () {
                                        model.navigateToSignUp();
                                        },
                                      style: kSubtitleTextStyle.copyWith(
                                          fontSize: 15,
                                          color: kOrange,
                                          fontWeight: FontWeight.bold),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
        viewModelBuilder: () => OnBoardingViewModel());
  }
}
