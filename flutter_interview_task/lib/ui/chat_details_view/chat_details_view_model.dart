import 'package:flutter/cupertino.dart';
import 'package:flutter_interview_task/app/app.locator.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class ChatDetailsViewModel extends BaseViewModel {
  final _navigationService = locator<NavigationService>();
  final controller = TextEditingController();

  void back() => _navigationService.back();
}
