import 'package:flutter/material.dart';
import 'package:flutter_chat_bubble/bubble_type.dart';
import 'package:flutter_chat_bubble/chat_bubble.dart';
import 'package:flutter_chat_bubble/clippers/chat_bubble_clipper_2.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:flutter_interview_task/constants/colors.dart';
import 'package:flutter_interview_task/constants/ui_helpers.dart';
import 'package:flutter_interview_task/models/chat_model.dart';
import 'package:flutter_interview_task/widgets/custom_textfield.dart';
import 'package:stacked/stacked.dart';

import 'chat_details_view_model.dart';

class ChatDetailsView extends StatelessWidget {
  final ChatModel chatModel;

  ChatDetailsView(this.chatModel);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ChatDetailsViewModel>.reactive(
        builder: (context, model, child) => Scaffold(
              resizeToAvoidBottomInset: true,
              backgroundColor: Colors.white,
              appBar: AppBar(
                centerTitle: false,
                elevation: 0,
                backgroundColor: Colors.white,
                title: Row(
                  children: [
                    CircleAvatar(backgroundImage: chatModel.image, radius: 20),
                    horizontalSpaceTiny,
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          chatModel.name,
                          style: kTitleTextStyle.copyWith(fontSize: 17),
                        ),
                        Row(
                          children: [
                            Container(
                              height: 6,
                              width: 6,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle, color: kGreen),
                            ),
                            horizontalSpaceTiny,
                            Text(
                              'Online',
                              style: kSubtitleTextStyle.copyWith(fontSize: 13),
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                actions: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Center(
                      child: Icon(
                        IconlyBold.call,
                        color: kBlack,
                      ),
                    ),
                  )
                ],
                leading: IconButton(
                    icon: Icon(Icons.arrow_back),
                    color: kBlack,
                    onPressed: model.back),
              ),
              body: Stack(
                children: [
                  SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        verticalSpaceSmall,
                        Text(
                          '1 April 12:00',
                          style: kSubtitleTextStyle.copyWith(fontSize: 13),
                        ),
                        ChatBubble(
                          backGroundColor: kOrange,
                          clipper:
                              ChatBubbleClipper2(type: BubbleType.sendBubble),
                          margin: EdgeInsets.only(left: 100),
                          child: Text(
                            r'Hey, Alex! Nice to meet you! I’d like to hire a walker and you’re perfect one for me. Can you help me out?',
                            style: kSubtitleTextStyle.copyWith(
                                fontSize: 13, color: Colors.white),
                          ),
                        ),
                        verticalSpaceSmall,
                        ChatBubble(
                          backGroundColor: Color(0xffECEEF1),
                          clipper: ChatBubbleClipper2(
                              type: BubbleType.receiverBubble),
                          child: Text(
                              'Hi! That’s great! Let me give you a call and we’ll discuss all the details',
                              style: kSubtitleTextStyle.copyWith(
                                  fontSize: 13, color: kBlack)),
                          margin: EdgeInsets.only(right: 100),
                        ),
                        verticalSpaceSmall,
                        Text(
                          '1 April 12:00',
                          style: kSubtitleTextStyle.copyWith(fontSize: 13),
                        ),
                        verticalSpaceSmall,
                        ChatBubble(
                          backGroundColor: kOrange,
                          margin: EdgeInsets.only(left: 300),
                          child: Text(
                            'Okay, I’m waiting for a call)',
                            style: kSubtitleTextStyle.copyWith(
                                fontSize: 13, color: Colors.white),
                          ),
                          clipper:
                              ChatBubbleClipper2(type: BubbleType.sendBubble),
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: kPadding,
                      child: Row(
                        children: [
                          Container(
                            height: 48,
                            width: 48,
                            child: Icon(
                              Icons.add,
                              color: kOrange,
                            ),
                            decoration: BoxDecoration(
                              borderRadius: kBorderRadius,
                              color: Color(0xffF5F5F5),
                            ),
                          ),
                          horizontalSpaceRegular,
                          Expanded(
                            child: CustomTextField.search(
                              color: Color(0xffF5F5F5),
                              height: 48.0,
                              labelText: 'Aa',
                              controller: model.controller,
                              suffixIcon: Icon(IconlyBold.voice, color: kBlack),
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
        viewModelBuilder: () => ChatDetailsViewModel());
  }
}
