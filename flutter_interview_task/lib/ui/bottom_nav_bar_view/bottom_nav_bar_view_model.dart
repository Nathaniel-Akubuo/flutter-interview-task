import 'package:flutter_interview_task/ui/chat/chats_view.dart';
import 'package:flutter_interview_task/ui/home/home_view.dart';
import 'package:flutter_interview_task/ui/moments/moments_view.dart';
import 'package:flutter_interview_task/ui/profile/profile_view.dart';
import 'package:stacked/stacked.dart';

class BottomNavBarViewModel extends IndexTrackingViewModel {
  var pages = [HomeView(),  MomentsView(),ChatsView(), ProfileView()];
}
