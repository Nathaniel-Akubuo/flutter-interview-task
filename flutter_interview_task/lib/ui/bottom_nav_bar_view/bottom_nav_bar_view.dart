import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:flutter_interview_task/constants/colors.dart';
import 'package:stacked/stacked.dart';

import 'bottom_nav_bar_view_model.dart';

class BottomNavBarView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<BottomNavBarViewModel>.reactive(
        builder: (context, model, child) => Scaffold(
          body: IndexedStack(
              index: model.currentIndex, children: model.pages),
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            elevation: 0,
            selectedItemColor: kBlack,
            currentIndex: model.currentIndex,
            unselectedItemColor: kMediumGrey,
            backgroundColor: Colors.white,
            onTap: model.setIndex,
            items: [
              BottomNavigationBarItem(
                  icon: Icon(IconlyBold.home), label: 'Home'),
              BottomNavigationBarItem(
                  icon: Icon(IconlyBold.user3),
                  label: 'Moments'),
              BottomNavigationBarItem(
                  icon: Icon(IconlyBold.send),
                  label: 'Chat'),
              BottomNavigationBarItem(
                  icon: Icon(IconlyBold.profile),
                  label: 'Profile'),
            ],
          ),
            ),
        viewModelBuilder: () => BottomNavBarViewModel());
  }
}
