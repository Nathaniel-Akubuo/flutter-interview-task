import 'package:flutter/cupertino.dart';
import 'package:flutter_interview_task/app/app.locator.dart';
import 'package:flutter_interview_task/app/app.router.dart';
import 'package:http/http.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class SignUpViewModel extends BaseViewModel {
  var fullName = TextEditingController();
  var email = TextEditingController();
  var password = TextEditingController();
  var isLoading = false;
  final _navigationService = locator<NavigationService>();

  Future signIn() async {
    setLoading(true);
    var response =
        await post(Uri.parse('https://hookb.in/mZZ8pmBdk6ilzXNNzQGp'));
    if (response.statusCode == 404) {
      _navigationService.navigateTo(Routes.bottomNavBarView);
      setLoading(false);
    } else {
      setLoading(false);
    }
  }

  void setLoading(value) {
    isLoading = value;
    notifyListeners();
  }
}
