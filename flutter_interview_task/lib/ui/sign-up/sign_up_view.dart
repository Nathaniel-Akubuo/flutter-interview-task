import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_interview_task/constants/colors.dart';
import 'package:flutter_interview_task/constants/ui_helpers.dart';
import 'package:flutter_interview_task/ui/sign-up/sign_up_view_model.dart';
import 'package:flutter_interview_task/widgets/custom_textfield.dart';
import 'package:flutter_interview_task/widgets/rounded_button.dart';
import 'package:stacked/stacked.dart';

class SignUpView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<SignUpViewModel>.reactive(
        builder: (context, model, child) => Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                elevation: 0,
                backgroundColor: Colors.white,
                leading: IconButton(
                  icon: Icon(Icons.arrow_back),
                  color: Colors.black,
                  iconSize: 30,
                  onPressed: () {},
                ),
              ),
              body: SingleChildScrollView(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Let's start here", style: kTitleTextStyle),
                    Text("Fill in your details to begin",
                        style: kSubtitleTextStyle),
                    verticalSpaceMedium,
                    CustomTextField.regular(
                        labelText: 'Fullname', controller: model.fullName),
                    verticalSpaceMedium,
                    CustomTextField.regular(
                        labelText: 'E-mail', controller: model.email),
                    verticalSpaceMedium,
                    CustomTextField.password(
                        labelText: 'Password', controller: model.password),
                    verticalSpaceMedium,
                    verticalSpaceSmall,
                    RoundedButton(
                      key: ValueKey('signInButton'),
                      onPressed: () async => model.signIn(),
                      child: model.isLoading
                          ? Center(
                              child: CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation(Colors.white),
                              ),
                            )
                          : Text(
                              'Sign up',
                              style: kSubtitleTextStyle.copyWith(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                    ),
                    verticalSpaceLarge,
                    verticalSpaceLarge,
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Center(
                        child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                            text: 'By signing in, I agree with the ',
                            style: kSubtitleTextStyle.copyWith(
                                fontSize: 15, color: kMediumGrey),
                            children: [
                              TextSpan(
                                text: 'Terms of Use ',
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {},
                                style: kSubtitleTextStyle.copyWith(
                                    fontSize: 15,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text: 'and ',
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {},
                                style: kSubtitleTextStyle.copyWith(
                                    fontSize: 15,
                                    color: kMediumGrey,
                                    fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text: 'Privacy Policy',
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {},
                                style: kSubtitleTextStyle.copyWith(
                                    fontSize: 15,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
        viewModelBuilder: () => SignUpViewModel());
  }
}
