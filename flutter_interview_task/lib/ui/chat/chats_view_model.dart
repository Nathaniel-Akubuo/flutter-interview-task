import 'package:flutter/cupertino.dart';
import 'package:flutter_interview_task/app/app.locator.dart';
import 'package:flutter_interview_task/app/app.router.dart';
import 'package:flutter_interview_task/models/chat_model.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class ChatsViewModel extends BaseViewModel {
  final _navigationService = locator<NavigationService>();

  var controller = TextEditingController();

  void navigateToChatDetails(chatModel) =>
      _navigationService.navigateTo(Routes.chatDetailsView,
          arguments: ChatDetailsViewArguments(chatModel: chatModel));
  List<ChatModel> list = [
    ChatModel(
        image: AssetImage('assets/dog1.jpg'),
        message: 'Hey! How’s your dog?',
        time: '1min',
        unread: true,
        name: 'Will Knowles'),
    ChatModel(
        image: AssetImage('assets/dog2.jpg'),
        message: 'Let’s go out!',
        time: '5h',
        name: 'Ryan Bond'),
    ChatModel(
        image: AssetImage('assets/dog3.jpg'),
        message: 'Hey! Long time no see',
        time: '1min',
        unread: true,
        name: 'Sirina Paul'),
    ChatModel(
        image: AssetImage('assets/dog4.jpg'),
        message: 'You fed the dog?',
        time: '6h',
        name: 'Sirina Paul'),
  ];
}
