import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:flutter_interview_task/constants/colors.dart';
import 'package:flutter_interview_task/widgets/chat_widget.dart';
import 'package:flutter_interview_task/widgets/custom_textfield.dart';
import 'package:stacked/stacked.dart';

import 'chats_view_model.dart';

class ChatsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ChatsViewModel>.reactive(
        builder: (context, model, child) => Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                leadingWidth: double.infinity,
                leading: Padding(
                  padding: EdgeInsets.only(left: 15),
                  child: Text('Chat',
                      style: kTitleTextStyle.copyWith(fontSize: 30)),
                ),
                backgroundColor: Colors.white,
                elevation: 0,
                centerTitle: false,
                bottom: PreferredSize(
                  preferredSize: Size.fromHeight(50),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: CustomTextField.search(
                      hintStyle: kSubtitleTextStyle.copyWith(fontSize: 17),
                      controller: model.controller,
                      prefixIcon: Icon(
                        IconlyLight.search,
                        color: kMediumGrey,
                      ),
                      labelText: 'Search',
                    ),
                  ),
                ),
              ),
              body: ListView.separated(
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (context, index) => GestureDetector(
                        onTap: () =>
                            model.navigateToChatDetails(model.list[index]),
                        child: ChatWidget(
                          chatModel: model.list[index],
                        ),
                      ),
                  separatorBuilder: (context, index) => SizedBox(
                        child: Divider(),
                        height: 20,
                      ),
                  itemCount: model.list.length),
            ),
        viewModelBuilder: () => ChatsViewModel());
  }
}
